# HOMEWORK 4 #

This README would normally document whatever steps are necessary to get your application up and running.

### MIDDLEWARE. FRAMEWORKS ###

### Tasks ###

* In a separate directory (e.g. http-servers) create three files called plain-text-server.js, html-server.js and json-server.js respectively. Implement basic http server using http module in each of them with the following requirements:

* For plain-text-server.js file:
- Set Content-Type header to plain text.

- Send “Hello World” string as a response.

* For html-server.js file:

- Create index.html file with the following content:

``

<html>
    <head></head>
    <body>
        <h1>{message}</h1>
    </body>
</html>

``

- Set Content-Type header to html.

- Read (readFileSync) an index.html file with fs module, replace message with a real message text.

- Send the response.

- Change readFileSync to be a readable stream and pipe it to response stream.

* For json-server.js file:

- Set Content-Type header to deal with JSON

- Take the following sample and send it as a JSON response:

``


const product = {
   id: 1,
   name: 'Supreme T-Shirt',
   brand: 'Supreme',
   price: 99.99,
   options: [
       { color: 'blue' },
       { size: 'XL' }
   ]
};



``

* Create an echo server in echo-server.js. Remember that request and response are streams so you can use all their power.
* Install express.
* Create an application structure or update an existing one to fit the following:


``


├── bin
├── config
├── controllers
├── helpers
├── middlewares
├── models
└── routes
├── app.js
├── index.js


``

* In index file import app from app.js, configure port and start an app


``

// index.js

import app from './app';
const port = process.env.PORT || 8080;
app.listen(port, () => console.log(`App listening on port ${port}!`))


``
* Create middleware for cookie parsing. 

 - Parsed cookie should be added to request stream object as parsedCookies field.

* Create middleware for query parsing. 

- Parsed query should be added to request stream object as parsedQuery field.


* Make your application to respond to the following routes:


``


URL
METHOD
ACTION
/api/products
GET
Return ALL products
/api/products/:id
GET
Return SINGLE product
/api/products/:id/reviews
GET
Return ALL reviews for a single product
/api/products
POST
Add NEW product and return it
/api/users
GET
Return ALL users


``

### Evaluation Criteria ###

* Application structure defined and some files are created. 

* All http servers are implemented following the requirements described in tasks 1 – 2.

* Express is installed and the main server files are created following the requirements described in tasks 3 – 5.

* Appropriate middleware from tasks 6 – 7 are implemented properly.

* Application responds to all routes described in task 8.


** Accidentally committed .idea directory files into git **
$ echo '.idea' >> .gitignore
$ git rm -r --cached .idea
$ git add .gitignore
$ git commit -m '(some message stating you added .idea to ignored entries)'
$ git push