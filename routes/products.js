const express = require('express');
const router = express.Router();
const uuidv1 = require('uuid/v1');
const getProducts = require('../controllers/products');
const parsedCookies = require('../middlewares/parsedCookies');
const parsedQuery = require('../middlewares/parsedQuery');

router.use(parsedCookies);
router.use(parsedQuery);

router.get('/', (req, res) => {
  res.json(getProducts('all'));
});

router.get('/:id', (req, res) => {
  res.json(getProducts('id', req.params.id));
});

router.get('/:id/reviews', (req, res) => {
  res.json(getProducts('reviews', req.params.id)[0].review);
});

// Send POST request with data:
// {
// "name": "Supreme T-Shirt 10",
//   "brand": "Supreme",
//   "price": "99.99",
//   "options": [
//   { "color": "blue" },
//   { "size": "XL" }
// ],
//   "review": "IMDb goes behind-the-scenes with movie prop master."
// }

router.post('/', (req, res) => {
  getProducts('post', uuidv1(), req.body);
  res.end();
});

module.exports = router;
