const express = require('express');
const router = express.Router();
const getUsers = require('../controllers/users');

router.get('/', (req, res) => {
  res.json(getUsers());
});

module.exports = router;


